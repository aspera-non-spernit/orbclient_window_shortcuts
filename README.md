# Orbclient Window Shortcuts

[![crates.io](https://img.shields.io/crates/v/orbclient_window_shortcuts.svg)](https://crates.io/crates/orbclient_window_shortcuts)
[![Build Status](https://travis-ci.org/aspera-non-spernit/orbclient_window_shortcuts.svg?branch=testing)](https://travis-ci.org/aspera-non-spernit/orbclient_window_shortcuts)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
![Rust Version](https://img.shields.io/badge/rust-nightly%202017--04--22-lightgrey.svg)


### Description

Shortcut support for `orbclient::Window`

The library makes used of:

```rust
#![feature(vec_remove_item)]
```
and therefore requires to be build with rust nightly. The default branch to be cloned from is 'testing'. Crates.io is the reliable source for a working lib. The documentation may not build and cause a failed build badge.

Please see:
[recent build logs](https://travis-ci.org/aspera-non-spernit/orbclient_window_shortcuts) for more details.

### Prerequisites

The orbclient requires sdl2 or orbital (redox os) to run, and may not be installed on your system. On Arch Linux your can use pacman to install libsdl2.

```linux
$ pacman -Syu sdl2
```

On Ubuntu you may install libsdl2 from a 3rd party or compile it from sources:

```linux
$ apt-get install libsdl2-dev
```
```linux
# wget https://www.libsdl.org/release/SDL2-2.0.5.tar.gz -O SDL2-2.0.5.tar.gz
# tar -xzvf SDL2-2.0.5.tar.gz
$ cd SDL2-2.0.5 && ./configure && make && sudo make install
```

### Implementation
The lib is available on creates.io. In your Cargo.toml:

```rust
orbclient_window_shortcuts = "0.1.5"
```

In your lib or bin code:

```rust
use shortcut::{Shortcut, ShortcutId, ShortcutEq};
```

Implementation for your `Window` application:

```rust
// Provides the capturing of default shortcuts: CTRL+O (Open), CTRL+N (New), CTRL-Q (Quit)
let mut sceq = ShortcutEq::with_defaults();
```

In a loop to capture window events implement EventOption::Key(ke):

Every keystroke that's a supported shortcut will return a `ShortcutId::{New, Open, Quit}`, which
can be matched.

```rust
events: loop {
    for event in window.events() {
        match event.to_option() {
            EventOption::Key(ke) => match sceq.update(ke) {
                Some(sc) => {
                    // Sample action
                    if PartialEq::eq(sc, &ShortcutId::Quit) { break 'events }
                None => {...},
           },
           ...
```

### The future and potential features

- Generic support to window specific shortcuts
- Enabling & disabling a set of shortcuts (adding and removing to supported shortcuts)
- Refacturing to a more rust idiomatic code
- A descriptive approach to declare shortcuts

I take any advice and ideas which I will considere when my rust programming capabilities improve. :)

### Contribution

Very much appreciated. Please file bug, enahncement or question [issues](https://github.com/aspera-non-spernit/orbclient_window_shortcuts/issues), or fork and request pulls. Thank you very much.

### Sorry

I am relatively new to rust and discover new things everyday. I apologise for the multiple versions per day on
crates.io failed builds, wrong badges, version numbers etc. :)
