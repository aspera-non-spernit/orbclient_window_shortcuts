use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, Error, Result};
use serde;
use serde_json;
use serde_derive;
use shortcut::Shortcuts;

pub fn load(path: &str) -> String {
    let mut contents = String::new();
    let file = File::open(path);
    match file {
        Ok(f) => {
            let mut buf_reader = BufReader::new(f);
            match buf_reader.read_to_string(&mut contents) {
                Ok(bytes) => {println!("{:?}", bytes)},
                Err(e) => {panic!("{:?}", e);},
            };
        },
        Err(e) => {panic!("{:?}", e);},
    };
    contents
}

fn de(string: &str) /*-> ShortcutComparator */{

}
