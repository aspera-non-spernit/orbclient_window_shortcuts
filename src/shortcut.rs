use serde;
use serde_json;
use serde_derive;
use orbclient::event::KeyEvent;

pub trait Comparator<S> {
    /// The comparator must be informed on_key_press and on_key_release The key_event
    /// contains that information. Generic over ShortcutId (enum variant)
    fn update(&mut self, key_event: KeyEvent) -> Option<&S>;
    /// Generic over ShortcutId (enum variant)
    fn compare(&mut self) -> Option<&S>;
    /// To make sure that there are no duplicates the vecctors containing the pressed keys
    /// must be sorted before dedup'ed.
    fn sort(&mut self);
}

#[derive(Debug)]
pub enum GenericShortcutId<S> {
    Id(S)
}

/// A shortcut consists of an id and a combination of keystrokes
/// id can be of any generic type
#[derive(Deserialize, Clone, Debug)]
pub struct Shortcut<S> {
    pub id: S,
    pub keys: Vec<u8>,
}

/// Pseudo shortcut ids
#[derive(PartialEq, Clone, Copy, Debug)]
pub enum PseudoId {
    Pressed
}

/// Most common shortcut ids.
#[derive(Deserialize, PartialEq, Clone, Copy, Debug)]
pub enum CommonId {
    New, Open, Save, Help, Quit
}

/// Container for the currently pressed keystrokes and of a set of supported shortcuts.
#[derive(Deserialize, Clone, Debug)]
pub struct Shortcuts<S> {
    /// A single Shortcut or None
    pub pressed: Vec<u8>,
    /// A set of Shortcuts or None
    pub supported: Option<Vec<Shortcut<S>>>,
    /// optional flag for the comparator
    pub min_shortcut_len: u8,
}
