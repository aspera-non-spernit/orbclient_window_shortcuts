//! Shortcut support for orbclient::window;

#![feature(type_ascription, vec_remove_item)]
#![allow(dead_code)]
extern crate orbclient;
extern crate serde;

#[macro_use]
extern crate serde_derive;
extern crate serde_json;
pub mod shortcut;
mod utils;
mod tests;

use utils::{load};

use std::fmt::Debug;
use std::cmp::{PartialEq, Eq};
use orbclient::{Window, EventOption};
use shortcut::{CommonId, Comparator, Shortcut, Shortcuts};

///Provides creators .
impl <S>Shortcuts<S> {
    pub fn new() -> Shortcuts<S> {
        Shortcuts {
            pressed: Vec::new(),
            supported: None,
            min_shortcut_len: 2,
        }
    }

    /// Takes a json description of a ShortcutComparator.
    /// For convenience there is a utils::load, but instead you should
    /// write your own load fn.
    pub fn from_json(json: &str) -> Shortcuts<S> {
        let mut sc = Shortcuts {
            pressed: Vec::new(),
            supported: None,
            min_shortcut_len: 2,
        };
        sc
    }

    /// If you want common shortcut support pass ShortcutEq::defaults() as custom argument
    pub fn with_custom(custom: Vec<Shortcut<S>>, min_shortcut_len: u8) -> Shortcuts<S> {
        let mut sc = Shortcuts {
            pressed: Vec::new(),
            supported: Some(custom),
            min_shortcut_len: min_shortcut_len,
        };
        sc.sort();
        sc
    }

    ///Enable a new 'set' of shortcuts after Shortcuts has been created.
    pub fn enable(&mut self, shortcuts: Vec<Shortcut<S>>) {
        match self.supported.as_mut() {
            None => {},
            Some(ssc) => {
                for shortcut in shortcuts {
                    ssc.push(shortcut);
                }
            }
        }
    }
}

/// Provides functionality to compare Shortcuts with pressed keys.
impl <S>Comparator<S> for Shortcuts<S> {
    fn compare(&mut self) -> Option<&S> {
        //println!("{:?} - {:?}", &self., &self.pressed);
        let mut pressed_match = None;
        match self.supported.as_mut() {
            //if no shortcuts supported no need to match keystrokes
            None => {},
            Some(shortcuts) => {
                //prerequisite for being acknowledge as a shortcut
                if self.pressed.len() >= self.min_shortcut_len as usize {
                    //for each supported shortcut
                    for ssc in shortcuts {
                        //can only be a match if number of keys are same length
                        //ie. supported CTRL+A != pressed ALT+CTRL+A
                        if ssc.keys.len() == self.pressed.len() {
                            if ssc.keys == self.pressed {
                                pressed_match = Some(&ssc.id);
                            }
                        }
                    }
                }
            }
        }
        pressed_match
    }

    fn sort(&mut self) {
        match self.supported.as_mut() {
            None => panic!("Wasn't able to assign shortcuts."),
            Some(scv) => {}
        };
    }

    /// updates the state of current key pressed and returns id if match
    fn update(&mut self, key_event: orbclient::event::KeyEvent) -> Option<&S> {
        let mut m = None;
        if key_event.pressed {
            self.pressed.push(key_event.scancode);
            self.pressed.sort();
            self.pressed.dedup();
            m = self.compare();
        } else if !key_event.pressed {
            self.pressed.remove_item(&key_event.scancode);
        }
        m
    }
}

impl <S>Shortcut<S> {
    pub fn new(scid: S, keys: Vec<u8>) -> Shortcut <S>{
        let s = Shortcut {
            id: scid,
            keys: keys,
        };
        s
    }
}
