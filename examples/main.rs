/* Commented to avoid compile errors an failed build badges on TravisCI,

extern crate orbclient;
extern crate orbclient_window_shortcuts;

use orbclient::{Window, Renderer, EventOption, Color};
use orbclient_window_shortcuts::shortcut::{Comparator, GenericShortcutId, Shortcut, CommonId, Shortcuts};

fn window() -> orbclient::Window {
    let (width, height) = orbclient::get_display_size().unwrap();
    let mut window = Window::new_flags(0, 0, width / 2, height / 2, "Shortcuts Example", &[orbclient::WindowFlag::Resizable]).unwrap();
    println!("This example does only support the Quit shortcut: CRTL+Q");
    window.sync();
    window
}

enum MyId {
    Increase, Decrease
}

*/
fn main() {


/*
    let mut window = window();

    let scs = Shortcuts::new();
    //Quit Shortcut with ShortcutId from CommonId
    let mut quit_k: Vec<u8> = Vec::new();
    quit_k.push(orbclient::event::K_CTRL);
    quit_k.push(orbclient::event::K_Q);
    quit_k.sort();

    let mut decrease_k = Vec::new();
    decrease_k.push(orbclient::event::K_CTRL);
    decrease_k.push(orbclient::event::K_MINUS);
    decrease_k.sort();

    let supported = Vec::new();

    supported.push(Shortcut{id: GenericShortcutId::Id(MyId::Decrease), keys: decrease_k});
    //Error: Type inferred from line above
    //supported.push(Shortcut{id: GenericShortcutId::Id(CommonId::Quit), keys: quit_k});

    scs.enable(supported);

    'events: loop {
        for event in window.events() {
            match event.to_option() {
                EventOption::Quit(qe) => break 'events,
                EventOption::Key(ke) => {
                    match scs.update(ke) {
                        Some(scid) => {
                            match scid {
                                &CommonId::Quit => {println!("{:?} returned.", scid); break 'events },
                                &CommonId::Open => {println!("{:?} not implemented.", scid)},
                                _ => {println!("{:?} unknown shortcut.", scid)}
                            };
                        },
                        None => {println!("No match")},
                    };
                }, //orbclient_window_shortcuts implementation
                _ => break
            };
        }
    }
*/
}
